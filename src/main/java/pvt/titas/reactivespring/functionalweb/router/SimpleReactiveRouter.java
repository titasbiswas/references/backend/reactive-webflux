package pvt.titas.reactivespring.functionalweb.router;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import pvt.titas.reactivespring.functionalweb.handler.SimpleReactiveHandler;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;

@Configuration
public class SimpleReactiveRouter {

    @Bean
    public RouterFunction<ServerResponse> route(SimpleReactiveHandler simpleReactiveHandler){
        return RouterFunctions
                .route(GET("/functional/flux").and(accept(MediaType.APPLICATION_JSON_UTF8)),
                        simpleReactiveHandler::handleFluxReq)
                .andRoute(GET("/functional/fluxstream").and(accept(MediaType.APPLICATION_STREAM_JSON))
                        , simpleReactiveHandler::handleFluxStreamReq)
                .andRoute(GET("/functional/mono").and(accept(MediaType.APPLICATION_JSON_UTF8))
                        , simpleReactiveHandler::handleMonoReq);

    }
}
