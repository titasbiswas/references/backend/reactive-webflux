package pvt.titas.reactivespring.functionalweb.router.v1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;
import pvt.titas.reactivespring.constants.ItemAppConstants;
import pvt.titas.reactivespring.functionalweb.handler.v1.ItemHandler;

import static org.springframework.web.reactive.function.server.RequestPredicates.*;

@Configuration
public class ItemRouter {

    @Bean
    public RouterFunction<ServerResponse> itemRoute(ItemHandler itemHandler) {
        return RouterFunctions
                .route(GET(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1).and(accept(MediaType.APPLICATION_JSON)),
                        itemHandler::getAllItem)
                .andRoute(GET(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1+"/{id}").and(accept(MediaType.APPLICATION_JSON)),
                        itemHandler::findOneItem)
                .andRoute(POST(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1).and(accept(MediaType.APPLICATION_JSON)).and(contentType(MediaType.APPLICATION_JSON)),
                        itemHandler::createItem)
                .andRoute(DELETE(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1+"/{id}").and(accept(MediaType.APPLICATION_JSON)),
                        itemHandler::deleteItemById)
                .andRoute(PUT(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1+"/{id}").and(accept(MediaType.APPLICATION_JSON)).and(contentType(MediaType.APPLICATION_JSON)),
                        itemHandler::updateItem);
    }
}
