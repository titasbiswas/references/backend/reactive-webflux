package pvt.titas.reactivespring.functionalweb.handler;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@Component
public class SimpleReactiveHandler {

    public Mono<ServerResponse> handleFluxReq(ServerRequest serverRequest){
        return ServerResponse
                .ok()
                .body(Flux.just(1, 2, 3, 4), Integer.class);
    }

    public Mono<ServerResponse> handleFluxStreamReq(ServerRequest serverRequest){
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_STREAM_JSON)
                .body(Flux.interval(Duration.ofSeconds(1)).log(), Long.class);
    }

    public Mono<ServerResponse> handleMonoReq(ServerRequest serverRequest) {
        return ServerResponse
                .ok()
                .body(Mono.just(1), Integer.class);
    }
}
