package pvt.titas.reactivespring.functionalweb.handler.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import pvt.titas.reactivespring.document.Item;
import pvt.titas.reactivespring.repository.ItemReactiveRepository;
import reactor.core.publisher.Mono;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

@Component
public class ItemHandler {

    private static Mono<ServerResponse> NOT_FOUND = ServerResponse.notFound().build();
    @Autowired
    private ItemReactiveRepository itemReactiveRepository;

    public Mono<ServerResponse> getAllItem(ServerRequest serverRequest) {
        return ServerResponse
                .ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(itemReactiveRepository.findAll(), Item.class);

    }

    public Mono<ServerResponse> findOneItem(ServerRequest serverRequest) {
        String id = serverRequest.pathVariable("id");
        Mono<Item> itemMono = itemReactiveRepository.findById(id);

        return itemMono.flatMap(item -> {
            return ServerResponse.ok()
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(fromValue(item));
        })
                .switchIfEmpty(NOT_FOUND);
    }

    public Mono<ServerResponse> createItem(ServerRequest serverRequest) {
        Mono<Item> itemMono = serverRequest.bodyToMono(Item.class);

        return itemMono.flatMap(item ->
                ServerResponse.status(HttpStatus.CREATED)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(itemReactiveRepository.save(item), Item.class));

    }

    public Mono<ServerResponse> deleteItemById(ServerRequest serverRequest) {
        String id = serverRequest.pathVariable("id");
        return itemReactiveRepository.deleteById(id)
                .flatMap(item -> ServerResponse.ok().body(item, Void.class));
    }

    public Mono<ServerResponse> updateItem(ServerRequest serverRequest) {
        String id = serverRequest.pathVariable("id");
        Mono<Item> itemMono = serverRequest.bodyToMono(Item.class);

        Mono<Item> updatedItemMono = itemMono.flatMap(item -> {
            return itemReactiveRepository.findById(id)
                    .flatMap(currentItem -> {
                        currentItem.setDescription(item.getDescription());
                        currentItem.setPrice(item.getPrice());
                        return itemReactiveRepository.save(currentItem);
                    });
        });

        return updatedItemMono.flatMap(item -> {
            return ServerResponse.ok()
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(fromValue(item));
        })
                .switchIfEmpty(NOT_FOUND);

    }

}
