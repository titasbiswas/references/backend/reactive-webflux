package pvt.titas.reactivespring.constants;

public class ItemAppConstants {

    public final static String ITEM_ENDPOINT_V1 = "/v1/items";
    public final static String ITEM_FUNC_ENDPOINT_V1 = "/v1/func/items";
    public static final String ITEM_STREAM_ENDPOINT_V1 = "/v1/stream/items";
}
