package pvt.titas.reactivespring.initializer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Component;
import pvt.titas.reactivespring.document.Item;
import pvt.titas.reactivespring.document.ItemStream;
import pvt.titas.reactivespring.repository.ItemReactiveRepository;
import pvt.titas.reactivespring.repository.ItemReactiveStreamRepository;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

@Component
@Slf4j
@Profile("!test")
public class ItemAppDataInitializer implements CommandLineRunner {

    @Autowired
    private ItemReactiveRepository itemReactiveRepository;

    @Autowired
    private ItemReactiveStreamRepository itemReactiveStreamRepository;

    @Autowired
    private MongoOperations mongoOperations;


    @Override
    public void run(String... args) throws Exception {
        initData();
        createCappedCollection();
        initDataStream();
    }

    private List<Item> data(){
        return Arrays.asList(new Item(null, "MBP 2016", 400.00),
                new Item(null, "Ipad Pro", 300.00),
                new Item(null, "Apple Watch 3", 499.99),
                new Item(null, "iPod Classic", 199.99),
                new Item("id5", "Airpod Pro", 199.99));
    }
    private void initData() {
        itemReactiveRepository.deleteAll()
                .thenMany(Flux.fromIterable(data()))
                .flatMap(itemReactiveRepository::save)
                .thenMany(itemReactiveRepository.findAll())
                .subscribe(item -> log.info("Item inserted from CommandLineRunner : "+item));
    }

    private void createCappedCollection(){
        mongoOperations.dropCollection(ItemStream.class);
        mongoOperations.createCollection(ItemStream.class, CollectionOptions.empty().maxDocuments(300).size(100).capped());
    }
    private void initDataStream() {
        Flux<ItemStream> itemStreamFlux = Flux.interval(Duration.ofMillis(700))
                .map(i -> new ItemStream(null, "ItemDesc" + i, 99.99 + i));

        itemReactiveStreamRepository
                .insert(itemStreamFlux)
                .subscribe(s -> log.info("Inserted ItemStream: "+s));
    }

}
