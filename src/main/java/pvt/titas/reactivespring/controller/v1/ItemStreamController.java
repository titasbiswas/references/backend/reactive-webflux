package pvt.titas.reactivespring.controller.v1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pvt.titas.reactivespring.document.Item;
import pvt.titas.reactivespring.document.ItemStream;
import pvt.titas.reactivespring.repository.ItemReactiveRepository;
import pvt.titas.reactivespring.repository.ItemReactiveStreamRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static pvt.titas.reactivespring.constants.ItemAppConstants.ITEM_STREAM_ENDPOINT_V1;

@Slf4j
@RestController
public class ItemStreamController {

    private ItemReactiveStreamRepository itemReactiveStreamRepository;

    @Autowired
    public ItemStreamController(ItemReactiveStreamRepository itemReactiveStreamRepository) {
        this.itemReactiveStreamRepository = itemReactiveStreamRepository;
    }

    @GetMapping(value = ITEM_STREAM_ENDPOINT_V1, produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    Flux<ItemStream> getAllItems() {
        return itemReactiveStreamRepository.findBy();
    }

    }
