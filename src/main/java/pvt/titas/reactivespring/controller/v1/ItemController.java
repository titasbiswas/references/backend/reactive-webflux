package pvt.titas.reactivespring.controller.v1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pvt.titas.reactivespring.document.Item;
import pvt.titas.reactivespring.repository.ItemReactiveRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static pvt.titas.reactivespring.constants.ItemAppConstants.ITEM_ENDPOINT_V1;

@Slf4j
@RestController
public class ItemController {

    private ItemReactiveRepository itemReactiveRepository;

    @Autowired
    public ItemController(ItemReactiveRepository itemReactiveRepository) {
        this.itemReactiveRepository = itemReactiveRepository;
    }

    @GetMapping(ITEM_ENDPOINT_V1)
    Flux<Item> getAllItems() {
        return itemReactiveRepository.findAll();
    }

    @GetMapping(ITEM_ENDPOINT_V1 + "/{id}")
    Mono<ResponseEntity<Item>> getOneItem(@PathVariable("id") String id) {

        return itemReactiveRepository.findById(id)
                .map(e -> new ResponseEntity<>(e, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(ITEM_ENDPOINT_V1)
    @ResponseStatus(HttpStatus.CREATED)
    Mono<Item> createItem(@RequestBody Item item) {
        return itemReactiveRepository.save(item);
    }

    @DeleteMapping(ITEM_ENDPOINT_V1 + "/{id}")
    Mono<Void> deleteItemById(@PathVariable("id") String id) {

        return itemReactiveRepository.deleteById(id);
    }

    @PutMapping(ITEM_ENDPOINT_V1 + "/{id}")
    Mono<ResponseEntity<Item>> getOneItem(@PathVariable("id") String id,
                                          @RequestBody Item item) {

        return itemReactiveRepository.findById(id)
                .flatMap(e -> {
                    e.setPrice(item.getPrice());
                    e.setDescription(item.getDescription());
                    return itemReactiveRepository.save(e);
                })
                .map(e -> new ResponseEntity<>(e, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

}
