package pvt.titas.reactivespring.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@RequestMapping("/reactiverest")
public class ReactiveRestController {

    @GetMapping("/flux")
    public Flux<Integer> flux(){
        return Flux.just(1, 2, 3, 4);
    }

    @GetMapping(value = "/fluxstream", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Long> fluxStream(){
        return Flux.interval(Duration.ofSeconds(1));
    }

    @GetMapping("/mono")
    public Mono<Integer> mono(){
        return Mono.just(1);
    }
}
