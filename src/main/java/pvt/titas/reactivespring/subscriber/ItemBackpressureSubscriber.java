package pvt.titas.reactivespring.subscriber;

import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Subscription;
import pvt.titas.reactivespring.document.Item;
import reactor.core.publisher.BaseSubscriber;

@Slf4j
public class ItemBackpressureSubscriber extends BaseSubscriber<Item> {
    int consumed, total;
    int batchSize, maxLimit;

    public ItemBackpressureSubscriber(int batchSize, int thresold){
        this.batchSize = batchSize;
        this.maxLimit = thresold;
    }

    @Override
    protected void hookOnSubscribe(Subscription subscription) {
        subscription.request(batchSize);
    }

    @Override
    protected void hookOnNext(Item value) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        log.info("--- Item consumed---"+value );
        consumed++;
        total++;
        if(consumed==batchSize){
            consumed=0;
            request(batchSize);
        }
        if(total==maxLimit)
            cancel();
    }
}
