package pvt.titas.reactivespring.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.stereotype.Repository;
import pvt.titas.reactivespring.document.Item;
import pvt.titas.reactivespring.document.ItemStream;
import reactor.core.publisher.Flux;

@Repository
public interface ItemReactiveStreamRepository extends ReactiveMongoRepository<ItemStream, String> {

    @Tailable
    Flux<ItemStream> findBy();
}
