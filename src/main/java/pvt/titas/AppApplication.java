package pvt.titas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pvt.titas.webflux.basic.EmployeeWebClient;

@SpringBootApplication
public class AppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppApplication.class, args);

        //EmployeeWebClient employeeWebClient = new EmployeeWebClient();
        //employeeWebClient.consume();
    }

}
