package pvt.titas.webflux.basic;

import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class EmployeeWebClient {

    WebClient webClient = WebClient.create("http://localhost:8080");

    public void consume() {
        Mono<Employee> empMono = webClient.get()
                .uri("/employees/{id}", 1)
                .retrieve()
                .bodyToMono(Employee.class);

        empMono.subscribe(
                System.out::println,
                error -> System.out.println(error.getMessage()),
                ()->System.out.println("Mono consumed"));;

        Flux<Employee> empFlux = webClient.get()
                .uri("/employees")
                .retrieve()
                .bodyToFlux(Employee.class);

        empFlux.subscribe(
                System.out::println,
                error -> System.out.println(error.getMessage()),
                ()->System.out.println("Flux consumed"));

    }

}
