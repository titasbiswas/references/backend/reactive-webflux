package pvt.titas.reactivespring.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@WebFluxTest
@DirtiesContext
public class ReactiveRestControllerTest {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void flux_stepVerifier() {
        Flux<Integer> integerFlux =  webTestClient.get()
                .uri("/reactiverest/flux")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .returnResult(Integer.class)
                .getResponseBody();

        StepVerifier.create(integerFlux)
                .expectSubscription()
                .expectNext(1, 2, 3, 4)
                .verifyComplete();
    }

    @Test
    public void flux_hasSize() {
        webTestClient.get()
                .uri("/reactiverest/flux")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Integer.class)
                .hasSize(4);


    }

    @Test
    public void flux_assert_returnResult() {
        List<Integer> expectedIntegerList = Arrays.asList(1, 2, 3, 4);

        EntityExchangeResult<List<Integer>> result = webTestClient.get()
                .uri("/reactiverest/flux")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Integer.class)
                .returnResult();
        assertEquals(expectedIntegerList, result.getResponseBody());

    }

    @Test
    public void flux_assert_consumeWith() {
        List<Integer> expectedIntegerList = Arrays.asList(1, 2, 3, 4);

        webTestClient.get()
                .uri("/reactiverest/flux")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Integer.class)
                .consumeWith(c ->{
                        assertEquals(expectedIntegerList, c.getResponseBody());
                }
                        );


    }

    @Test
    public void fluxStream() {
        Flux<Long> integerFlux =  webTestClient.get()
                .uri("/reactiverest/fluxstream")
                .accept(MediaType.APPLICATION_STREAM_JSON)
                .exchange()
                .expectStatus().isOk()
                .returnResult(Long.class)
                .getResponseBody();

        StepVerifier.create(integerFlux)
                .expectSubscription()
                .expectNext(0L,1L, 2L, 3L, 4L)
                .thenCancel()
                .verify();
    }

    @Test
    public void mono_stepVerifier() {
        webTestClient.get()
                .uri("/reactiverest/mono")
                .accept(MediaType.APPLICATION_JSON_UTF8)
                .exchange()
                .expectStatus().isOk()
                .expectBody(Integer.class)
                .consumeWith(c -> assertEquals(new Integer(1), c.getResponseBody()));


    }
}