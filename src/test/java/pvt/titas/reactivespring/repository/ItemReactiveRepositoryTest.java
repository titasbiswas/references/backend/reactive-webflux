package pvt.titas.reactivespring.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import pvt.titas.reactivespring.document.Item;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

@DataMongoTest
@RunWith(SpringRunner.class)
@DirtiesContext
public class ItemReactiveRepositoryTest {

    @Autowired
    private ItemReactiveRepository itemReactiveRepository;

    List<Item> items = Arrays.asList(new Item(null, "MBP 2016", 400.00),
            new Item(null, "Ipad Pro", 300.00),
            new Item(null, "Apple Watch 3", 499.99),
            new Item(null, "iPod Classic", 199.99),
            new Item("id5", "Airpod Pro", 199.99));

    @Before
    public void setUp(){
        itemReactiveRepository.deleteAll()
                .thenMany(Flux.fromIterable(items))
                .flatMap(itemReactiveRepository::save)
                .doOnNext(e -> System.out.println("Inserted Item: "+e))
                .blockLast();
    }

    @Test
    public void testFindAll() {
        StepVerifier.create(itemReactiveRepository.findAll().log())
                .expectSubscription()
                .expectNextCount(5)
                .verifyComplete();
    }

    @Test
    public void testFindById() {
        StepVerifier.create(itemReactiveRepository.findById("id5"))
                .expectSubscription()
                .expectNextMatches(item -> item.getDescription().equals("Airpod Pro"))
                .verifyComplete();
    }

    @Test
    public void testFindByDescription() {
        StepVerifier.create(itemReactiveRepository.findByDescription("Ipad Pro"))
                .expectSubscription()
                .expectNextMatches(item -> item.getPrice().equals(300.00))
                .verifyComplete();
    }

    @Test
    public void testSave() {
        Item item = new Item(null, "Time Machine", 499.99);
        StepVerifier.create(itemReactiveRepository.save(item))
                .expectSubscription()
                .expectNextMatches(i -> i.getPrice().equals(499.99))
                .verifyComplete();
    }

    @Test
    public void testUpdateById() {

        Mono<Item> item = itemReactiveRepository.findById("id5")
                .map(i ->{
                    i.setPrice(449.99);
                    return i;
                } )
                .flatMap(itemReactiveRepository::save);

        StepVerifier.create(item)
                .expectSubscription()
                .expectNextMatches(i -> i.getPrice()==449.99)
                .verifyComplete();

    }

    @Test
    public void testDeleteById() {

        Mono<Void> item = itemReactiveRepository.findById("id5")
                .map(i -> i.getId())
                .flatMap(itemReactiveRepository::deleteById);

        StepVerifier.create(item)
                .expectSubscription()
                .expectNextCount(0)
                .verifyComplete();

    }

    @Test
    public void testDelete() {

        Mono<Void> item = itemReactiveRepository.findById("id5")
                .flatMap(itemReactiveRepository::delete);

        StepVerifier.create(item)
                .expectSubscription()
                .expectNextCount(0)
                .verifyComplete();

    }

}