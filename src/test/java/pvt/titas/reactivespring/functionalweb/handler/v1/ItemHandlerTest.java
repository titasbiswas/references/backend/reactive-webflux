package pvt.titas.reactivespring.functionalweb.handler.v1;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import pvt.titas.reactivespring.constants.ItemAppConstants;
import pvt.titas.reactivespring.document.Item;
import pvt.titas.reactivespring.repository.ItemReactiveRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@SpringBootTest
@RunWith(SpringRunner.class)
@DirtiesContext
@ActiveProfiles("test")
@AutoConfigureWebTestClient
@Slf4j
public class ItemHandlerTest {

    @Autowired
    ItemReactiveRepository itemReactiveRepository;

    @Autowired
    WebTestClient webTestClient;

    private List<Item> data() {
        return Arrays.asList(new Item(null, "MBP 2016", 400.00),
                new Item(null, "Ipad Pro", 300.00),
                new Item(null, "Apple Watch 3", 499.99),
                new Item(null, "iPod Classic", 199.99),
                new Item("id5", "Airpod Pro", 199.99));
    }

    @Before
    public void initData() {
        itemReactiveRepository.deleteAll()
                .thenMany(Flux.fromIterable(data()))
                .flatMap(itemReactiveRepository::save)
                .doOnNext(item -> log.info("Item inserted from " + log.getClass() + " : " + item))
                .blockLast();
    }

    @Test
    public void getAllItems() {
        webTestClient.get()
                .uri(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Item.class)
                .hasSize(5);
    }

    @Test
    public void getAllItems_consumeWith() {
        webTestClient.get()
                .uri(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Item.class)
                .hasSize(5)
                .consumeWith(elements -> {
                    elements.getResponseBody().forEach(e -> assertNotNull(e.getId()));
                });
    }

    @Test
    public void getAllItems_StepVerifier() {
        Flux<Item> itemFlux = webTestClient.get()
                .uri(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .returnResult(Item.class)
                .getResponseBody();

        StepVerifier.create(itemFlux)
                .expectSubscription()
                .expectNextCount(5)
                .verifyComplete();
    }

    @Test
    public void getOneItem() {
        webTestClient.get()
                .uri(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1.concat("/{id}"), "id5")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.price", 199.99);

    }

    @Test
    public void getOneItem_NotFound() {
        webTestClient.get()
                .uri(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1.concat("/{id}"), "junk")
                .exchange()
                .expectStatus().isNotFound();

    }

    @Test
    public void createItem() {

        webTestClient.post()
                .uri(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1)
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(new Item(null, "Apple TV", 299.99)), Item.class)
                .exchange()
                .expectStatus().isCreated()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.description").isEqualTo("Apple TV")
                .jsonPath("$.price").isEqualTo(299.99);
    }

    @Test
    public void deleteItem() {
        webTestClient.delete()
                .uri(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1.concat("/{id}"), "junk")
                .exchange()
                .expectStatus().isOk();

    }

    @Test
    public void updateItem() {

        webTestClient.put()
                .uri(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1.concat("/{id}"), "id5")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(new Item("id5", "Airpod Pro", 149.99)), Item.class)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isEqualTo("id5")
                .jsonPath("$.price").isEqualTo(149.99);
    }

    @Test
    public void updateItem_NotFound() {

        webTestClient.put()
                .uri(ItemAppConstants.ITEM_FUNC_ENDPOINT_V1.concat("/{id}"), "junk")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(new Item(null, "Airpod Pro", 149.99)), Item.class)
                .exchange()
                .expectStatus().isNotFound();
    }

}