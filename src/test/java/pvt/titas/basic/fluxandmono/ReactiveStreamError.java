package pvt.titas.basic.fluxandmono;

import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;

public class ReactiveStreamError {

    @Test
    public void test_onErrorResume() {
        Flux<String> stringFlux = Flux.just("A", "B")
                .concatWith(Flux.error(new RuntimeException()))
                .concatWith(Flux.just("C"))
                .onErrorResume(e -> {
                    System.out.println("Error Occured" + e);
                    return Flux.just("default");
                });

        StepVerifier.create(stringFlux)
                .expectNext("A", "B")
                .expectNext("default")
                //.expectError(RuntimeException.class)
                //.expectNext("A", "B")
                .verifyComplete();
    }

    @Test
    public void test_onErrorReturn() {
        Flux<String> stringFlux = Flux.just("A", "B")
                .concatWith(Flux.error(new RuntimeException()))
                .concatWith(Flux.just("C"))
                .onErrorReturn("default");

        StepVerifier.create(stringFlux)
                .expectNext("A", "B")
                .expectNext("default")
                //.expectError(RuntimeException.class)
                //.expectNext("A", "B")
                .verifyComplete();
    }

    @Test
    public void test_onErrorContinue() throws InterruptedException {
        Flux<String> stringFlux = Flux.just("A", "B")
                .concatWith(Flux.error(new RuntimeException()))
                .concatWith(Flux.just("C"))
                .onErrorContinue((t1, t2) -> System.out.println(t1 + " After Error " + t2));

        StepVerifier.create(stringFlux.log())
                .expectNext("A", "B")
                //.expectNext("default")
                .expectError(RuntimeException.class)
                //.verifyComplete()
                .verify()
        ;

        Thread.sleep(3000);
    }

    @Test
    public void test_onErrorMap() {
        Flux<String> stringFlux = Flux.just("A", "B")
                .concatWith(Flux.error(new RuntimeException()))
                .concatWith(Flux.just("C"))
                .onErrorMap(e -> new CustomException(e));

        StepVerifier.create(stringFlux)
                .expectNext("A", "B")
                //.expectNext("default")
                .expectError(CustomException.class)
                .verify();
    }

    @Test
    public void test_onErrorMap_Retry() {
        Flux<String> stringFlux = Flux.just("A", "B")
                .concatWith(Flux.error(new RuntimeException()))
                .concatWith(Flux.just("C"))
                .onErrorMap(e -> new CustomException(e))
                .retry(2);

        StepVerifier.create(stringFlux)
                .expectNext("A", "B")
                .expectNext("A", "B")
                .expectNext("A", "B")
                //.expectNext("default")
                .expectError(CustomException.class)
                .verify();
    }

    @Test
    public void test_onErrorMap_RetryBackOff() {
        Flux<String> stringFlux = Flux.just("A", "B")
                .concatWith(Flux.error(new RuntimeException()))
                .concatWith(Flux.just("C"))
                .onErrorMap(e -> new CustomException(e))
                .retryBackoff(2, Duration.ofSeconds(2));

        StepVerifier.create(stringFlux)
                .expectNext("A", "B")
                .expectNext("A", "B")
                .expectNext("A", "B")
                //.expectNext("default")
                .expectError(CustomException.class)
                .verify();
    }
}
