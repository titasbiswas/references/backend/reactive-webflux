package pvt.titas.basic.fluxandmono;

import org.junit.Test;
import reactor.core.publisher.ConnectableFlux;
import reactor.core.publisher.Flux;

import java.time.Duration;

public class ReactiveStreamHotnColdPublisher {

    @Test
    public void coldPublisher() throws InterruptedException {
        Flux<Integer> integerFlux = Flux.range(1,5)
                .delayElements(Duration.ofSeconds(1))
                .log();

        integerFlux.subscribe(System.out::println);

        Thread.sleep(2000);

        integerFlux.subscribe(System.out::println);
        Thread.sleep(3000);
    }

    @Test
    public void hotPublisher() throws InterruptedException {
        Flux<Integer> integerFlux = Flux.range(1,5)
                .delayElements(Duration.ofSeconds(1))
                .log();

        ConnectableFlux<Integer> integerConnectableFlux = integerFlux.publish();
        integerConnectableFlux.connect();

        integerConnectableFlux.subscribe(s -> System.out.println("Subscriber 1 "+s));
        Thread.sleep(3000);

        integerConnectableFlux.subscribe(s -> System.out.println("Subscriber 2 "+s));
        Thread.sleep(5000);
    }
}
