package pvt.titas.basic.fluxandmono;

import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

public class FluxAndMonoTestBasics {

    @Test
    public void testFluxCreate(){
        Flux<String> stringFlux = Flux.just("Spring", "Boot", "Webflux")
                .concatWith(Flux.error(new RuntimeException("Flux error occurred !")));

        stringFlux.subscribe(str -> System.out.println(str),
                e -> System.err.println("Error is: "+e),
                () -> System.out.println("Completed")
        );
    }

    @Test
    public void testFlux(){
        Flux<String> stringFlux = Flux.just("Spring", "Boot", "Webflux").log();

        StepVerifier.create(stringFlux)
                .expectNext("Spring")
                .expectNext("Boot")
                .expectNext("Webflux")
                .verifyComplete();
    }
    @Test
    public void testFlux_withError(){
        Flux<String> stringFlux = Flux.just("Spring", "Boot", "Webflux")
                .concatWith(Flux.error(new RuntimeException("Flux error occurred !")));

        StepVerifier.create(stringFlux.log())
                .expectNext("Spring")
                .expectNext("Boot")
                .expectNext("Webflux")
                .expectError(RuntimeException.class)
                .verify();
    }

    @Test
    public void monoTest(){
        Mono<Integer> integerMono = Mono.just(1);

        StepVerifier.create(integerMono)
                .expectNext(1).verifyComplete();
    }

    @Test
    public void monoTest_withError(){
        Mono<Integer> integerMono = Mono.error(new RuntimeException("Mono Error occurred"));

        StepVerifier.create(integerMono)
                .expectError().verify();
    }
}
