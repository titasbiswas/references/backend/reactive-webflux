package pvt.titas.basic.fluxandmono;

import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;

public class ReactiveStreamCombine {

    @Test //executes parallel, no order maintained
    public void testUsing_Merge(){
        Flux<String> stringFlux1 = Flux.just("A", "B", "C").delayElements(Duration.ofSeconds(1));
        Flux<String> stringFlux2 = Flux.just("D", "E", "F");

        Flux<String> mergedFlux = Flux.merge(stringFlux1, stringFlux2);

        StepVerifier.create(mergedFlux.log())
                .expectSubscription()
                .expectNextCount(6)
                //.expectNext("A", "B", "C","D", "E", "F" )
                .verifyComplete();
    }

    @Test //executes parallel,  order maintained
    public void testUsing_Merge_Ordered_Sequential(){
        Flux<String> stringFlux1 = Flux.just("A", "B", "C").delayElements(Duration.ofSeconds(1));
        Flux<String> stringFlux2 = Flux.just("D", "E", "F");

        //Flux<String> mergedFlux = Flux.mergeOrdered(stringFlux1, stringFlux2);
        Flux<String> mergedFlux = Flux.mergeSequential(stringFlux1, stringFlux2);


        StepVerifier.create(mergedFlux.log())
                .expectSubscription()
                //.expectNextCount(6)
                .expectNext("A", "B", "C","D", "E", "F" )
                .verifyComplete();
    }

    @Test //executes parallel,  order maintained
    public void testUsing_Concat(){
        Flux<String> stringFlux1 = Flux.just("A", "B", "C").delayElements(Duration.ofSeconds(1));
        Flux<String> stringFlux2 = Flux.just("D", "E", "F");

        //Flux<String> mergedFlux = Flux.mergeOrdered(stringFlux1, stringFlux2);
        Flux<String> mergedFlux = Flux.concat(stringFlux1, stringFlux2);


        StepVerifier.create(mergedFlux.log())
                .expectSubscription()
                //.expectNextCount(6)
                .expectNext("A", "B", "C","D", "E", "F" )
                .verifyComplete();
    }

    @Test //executes parallel,  order maintained
    public void testUsing_Zip() throws InterruptedException {
        Flux<String> stringFlux1 = Flux.just("A", "B", "C", "D");
        Flux<String> stringFlux2 = Flux.just("D", "E", "F").delayElements(Duration.ofSeconds(1));

        //Flux<String> mergedFlux = Flux.mergeOrdered(stringFlux1, stringFlux2);
        Flux<String> mergedFlux = Flux.zip(stringFlux1, stringFlux2, (e1, e2)->e1+e2);


        StepVerifier.create(mergedFlux.log())
                .expectSubscription()
                .expectNextCount(3)
                //.expectNext("A", "B", "C","D", "E", "F" )
                .verifyComplete();
        Thread.sleep(4000);
    }


}
