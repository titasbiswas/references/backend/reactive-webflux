package pvt.titas.basic.fluxandmono;

import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import static reactor.core.scheduler.Schedulers.parallel;

public class ReactiveStreamTransform {

    List<String> list = Arrays.asList("Martha", "Marcy", "May", "Marlene");

    @Test
    public void fluxMapAndFilter(){
        Flux<String> stringFlux= Flux.fromIterable(list)
                .map(String::toUpperCase)
                .filter(s -> s.length()>3);

        StepVerifier.create(stringFlux)
                .expectNext("MARTHA", "MARCY", "MARLENE")
        .verifyComplete();

    }

    @Test
    public void fluxFlaMap(){
        Flux<String> stringFlux= Flux.fromIterable(list)
                .flatMap(s-> Flux.fromIterable(Arrays.asList(s, "is awesome")))
                ;

        StepVerifier.create(stringFlux)
                .expectNext("Martha","is awesome","Marcy", "is awesome","May", "is awesome", "Marlene", "is awesome")
                .verifyComplete();

    }

    @Test
    public void fluxFlaMap_Parallel(){
        Flux<String> stringFlux= Flux.fromIterable(list)
                .window(2)
                .flatMap(s->
                            s
                            //.map(e -> Arrays.asList(e))
                            .subscribeOn(parallel())
                            //.flatMap(e -> Flux.fromIterable(e))
                            .flatMap(e -> Flux.just(e))
                ).log();

        StepVerifier.create(stringFlux)
                .expectNextCount(4)
                .verifyComplete();

    }

    @Test
    public void fluxFlaMap_Parallel_seq(){
        Flux<String> stringFlux= Flux.fromIterable(list)
                .window(2)
                .flatMapSequential(s->
                        s
                                //.map(e -> Arrays.asList(e))
                                .subscribeOn(parallel())
                                //.flatMap(e -> Flux.fromIterable(e))
                                .flatMap(e -> Flux.just(e))
                )
                .map(String::toUpperCase)
                .log();

        StepVerifier.create(stringFlux)
                .expectNext("MARTHA", "MARCY", "MAY","MARLENE")
                .verifyComplete();

    }


}
