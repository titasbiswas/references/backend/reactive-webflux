package pvt.titas.basic.fluxandmono;

public class CustomException extends Throwable {
    public CustomException(Throwable e) {
        super(e);
    }
}
