package pvt.titas.basic.fluxandmono;

import org.junit.Test;
import reactor.core.publisher.BaseSubscriber;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import java.time.Duration;

public class ReactiveStreamBackPressure {

    @Test
    public void backPressureTest() {

        Flux<Integer> finiteFlux = Flux.range(1, 10)
                .log();

        StepVerifier.create(finiteFlux)
                .expectSubscription()
                .thenRequest(1)
                .expectNext(1)
                .thenRequest(1)
                .expectNext(2)
                .thenCancel()
                .verify();

    }

   @Test
    public void backPressure(){
       Flux<Integer> integerFlux = Flux.range(1,5)
               //.delayElements(Duration.ofSeconds(1))
               .log();
       integerFlux.subscribe(e-> System.out.println(e),
               (error)-> System.err.println(error),
               ()-> System.out.println("Completed"),
               (subscription)->{
           subscription.request(3);
                   try { Thread.sleep(2000); } catch (InterruptedException e) { e.printStackTrace(); }
                   subscription.request(2);
               });

   }

    @Test
    public void backPressure_cancel(){
        Flux<Integer> integerFlux = Flux.range(1,5)
                //.delayElements(Duration.ofSeconds(1))
                .log();
        integerFlux.subscribe(e-> System.out.println(e),
                (error)-> System.err.println(error),
                ()-> System.out.println("Completed"),
                (subscription)->{
                    subscription.request(3);
                    subscription.cancel();
                    try { Thread.sleep(2000); } catch (InterruptedException e) { e.printStackTrace(); }
                    subscription.request(2);
                });

    }

    @Test
    public void backPressure_cancel_onCondition(){
        Flux<Integer> integerFlux = Flux.range(1,5)
                //.delayElements(Duration.ofSeconds(1))
                .log();
        integerFlux.subscribe(new BaseSubscriber<Integer>() {
            @Override
            protected void hookOnNext(Integer value) {
                System.out.println(value);
                if (value==5)
                    cancel();
            }
        });

    }
}
