package pvt.titas.basic.fluxandmono;

import org.junit.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class FluxAndMonoSources {

    List<String> list = Arrays.asList("Martha", "Marcy", "May", "Marlene");

    @Test
    public void fluxFromIterables(){
        Flux.fromIterable(list)
                .subscribe(System.out::println);
    }

    @Test
    public void fluxFromStream(){
        Flux.fromStream(list.stream())
                .subscribe(System.out::println);
    }

    @Test
    public void fluxFromArray(){
        Flux.fromArray(new String[]{"Martha", "Marcy", "May", "Marlene"})
                .subscribe(System.out::println);
    }

    @Test
    public void fluxFromRange(){
        Flux.range(1,5)
                .subscribe(System.out::println);
    }

    @Test
    public void monoEmpty(){
        Mono.justOrEmpty(null).subscribe(System.out::println);
    }

    @Test
    public void monoFromSupplier(){
        Supplier<String> stringSupplier = ()->{
            return "Marcy";
        };
        Mono<String> stringMono = Mono.fromSupplier(stringSupplier);

        StepVerifier.create(stringMono.log())
                .expectNext("Marcy")
                .verifyComplete();
    }
}
